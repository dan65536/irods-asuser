#!/bin/bash

mk_irods_auth_dir() {

# first argument is irods user login
# optional 2nd arg is alternate name for as_user (user-hostname if you like)

  local USER
  USER=$1 ; [ -n "$USER" ] || return 
  local MONIKER=${2:-$USER}
  [ -d .irods.$MONIKER ] && { echo >&2 "auth dir for $MONIKER already exists"; exit 123; }
  read -sp "password for user '$USER'->" pw
  shift
  local IRODS_AUTHENTICATION_FILE IRODS_ENVIRONMENT_FILE
  unset IRODS_AUTHENTICATION_FILE IRODS_ENVIRONMENT_FILE
  BACKUP=~/.irods.bak.$$
  bash -c "
    [ -e $BACKUP ] && rm -fr $BACKUP 2>/dev/null  
    [ -e $BACKUP ] && { echo >&2 'Could not delete backup .irods directory' ; exit 2; }
    { [ ! -d ~/.irods ] || mv ~/.irods $BACKUP; } && \
      trap -- '[ -d $BACKUP -a ! -d ~/.irods ] && mv $BACKUP ~/.irods # set hook for clean-up
      [ "$VERB" -ge 2 ] && echo >&2 unbak-on-exit' EXIT
    iinit >/dev/null 2>&1 <<-EOF
	${IRODS_SVR_HOSTNAME:=`hostname`}
	${IRODS_SVR_PORT:=1247}
	${IRODS_USER_NAME:=$USER}
	${IRODS_ZONE_NAME:=tempZone}
	${pw}
	EOF
    # ^--- must be tab-prefixed
    # test iinit return code:
    if [ \$? -eq 0 ] ;then
        echo >&2 -n $'\\nSUCCESS: entry created for: $MONIKER'  && \
        mv ~/.irods{,.$MONIKER}  && echo >&2 ' ***' Y
    else
        echo >&2 $'\\nFAILURE: Error in iinit: does $USER exist?'
        [ -d ~/.irods ] && \
          [ ! -r  ~/.irods/irods_environment.json ] && \
          rm -fr ~/.irods
    fi
  " 
  [ "$VERB" -ge 1 ] && echo >&2 "--- end mk_irods_auth_dir()"
}

as_user() {
  local FULLPATH=$(readlink -e $1); shift
  local AUTH=($FULLPATH/.irods*)
  local ENV=($FULLPATH/irods_*.json)

  [ ${#AUTH[*]} -eq 1 ] && [ ${#ENV[*]} -eq 1 ] ||  { echo >&2 "wrong # of irods file" ; exit 2 ; }

  export IRODS_AUTHENTICATION_FILE="${AUTH[0]}"
  export IRODS_ENVIRONMENT_FILE="${ENV[0]}"

  { [[ $IRODS_AUTHENTICATION_FILE = *\** || \
       $IRODS_ENVIRONMENT_FILE = *\** ]] ;} && { echo >&2 "one or more files missing"; exit 3
  }

  [ -n "$VERBOSE" ] && {
  echo "\
    IRODS_AUTHENTICATION_FILE = $IRODS_AUTHENTICATION_FILE 
    IRODS_ENVIRONMENT_FILE    = $IRODS_ENVIRONMENT_FILE "
  } >&2

  "$@"
}

asuser()
{
    local DIREXT=$1
    [ -n "$DIREXT" ] && { shift; 
	    as_user ~/.irods.$DIREXT "$@"
    }
}

VERB=0

if [ $# -ge 1 ] ; then

  while : ; do
    case $1 in
      "-h") IRODS_SVR_HOSTNAME=$2; shift 2;;
      "-p") IRODS_SVR_PORT=$2;     shift 2;;
      "-z") IRODS_ZONE_NAME=$2; shift 2;;
      "-V") ((VERB=0+$2)); shift 2;;
      "-v") VERB=1; shift 1;;
      *) break;;
    esac
  done
  mk_irods_auth_dir "$1" ${2+"$2"}
  [ "$VERB" -ge 1 ] && echo >&2 "( firstarg $1 status $? -- )"

else

  echo >&2 "usage: $0 <irods_username>"; exit 1

fi
