# as_user

   - clone under ``$HOME`
   - Make symlink ~/as_user.sh to the corresponding file within this repo
   - Add to ~/.bashrc
```
asuser () {
    ARG=$1;
    [ -n "$ARG" ] && {
        shift;
        ~/as_user.sh ~/.irods.$ARG "$@"
    }
}
ignoreeof() {
    LVL=${1:-1}
    [ $# -gt 0 ] && [ -z "$1" ] && { echo >&2 "SHLVL=$SHLVL" ; return ; }
    [ $SHLVL -le $LVL ] && IGNOREEOF=11
}
alias ignoreeof_1='[ $SHLVL -le 1 ] && IGNOREEOF=11'
alias ignoreeof_2='[ $SHLVL -le 2 ] && IGNOREEOF=11'
```
