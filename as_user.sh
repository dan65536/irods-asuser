#!/bin/bash
case "$1" in
  -[xX]) x='
asuser () { 
    ARG=$1;
    [ -n "$ARG" ] && { 
        shift;
        ~/as_user.sh ~/.irods.$ARG "$@"
    }
}
ignoreeof() {
    LVL=${1:-1}
    [ $# -gt 0 ] && [ -z "$1" ] && { echo >&2 "SHLVL=$SHLVL" ; return ; }
    [ $SHLVL -le $LVL ] && IGNOREEOF=11
}'"
alias ignoreeof_1='[ \$SHLVL -le 1 ] && IGNOREEOF=11'
alias ignoreeof_2='[ \$SHLVL -le 2 ] && IGNOREEOF=11'"
   echo "$x" ; exit ;;

   *) if [ ! -d "$1" ] ; then echo >&2 "'$1' is not a directory" ; exit 1 
      else DIR="$1"; shift
      fi ;;
esac

#-----

ABS=$(readlink -e $DIR)
AUTH=($ABS/.irods*)
ENV=($ABS/irods_*.json)

[ ${#AUTH[*]} -eq 1 ] && [ ${#ENV[*]} -eq 1 ] ||  { echo >&2 "wrong # of irods file" ; exit 2 ; }

export IRODS_AUTHENTICATION_FILE="${AUTH[0]}"
export IRODS_ENVIRONMENT_FILE="${ENV[0]}"

{ [[ $IRODS_AUTHENTICATION_FILE = *\** || \
     $IRODS_ENVIRONMENT_FILE = *\** ]] ;} && { echo >&2 "one or more files missing"; exit 3
}

[ -n "$VERBOSE" ] && {
echo "\
  IRODS_AUTHENTICATION_FILE = $IRODS_AUTHENTICATION_FILE 
  IRODS_ENVIRONMENT_FILE    = $IRODS_ENVIRONMENT_FILE "
} >&2

exec "$@"
